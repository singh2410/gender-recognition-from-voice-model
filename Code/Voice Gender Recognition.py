#!/usr/bin/env python
# coding: utf-8

# # Gender Recognition from voice
# #By- Aarush Kumar
# #Dated: October 20,2021

# In[1]:


from IPython.display import Image
Image(url='https://miro.medium.com/max/634/1*RTYreJ-PHBj2S33Eif2acA.jpeg')


# In[2]:


import numpy as np 
import pandas as pd
from matplotlib import pyplot as plt 
import seaborn as sns 
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix, roc_curve, roc_auc_score,auc, accuracy_score


# In[3]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/Voice Gender Recog/Data/voice.csv')


# In[4]:


df


# In[5]:


df.columns


# In[6]:


df.isnull().sum()


# In[7]:


df.describe()


# In[8]:


df.describe().T


# In[9]:


from IPython.core.display import HTML 
display(HTML(df.head(10).to_html()))


# In[10]:


df['label'] = df['label'].map({"male":0,"female":1})


# In[11]:


y = df['label'].values
X = df.drop(['label'], axis=1).values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)


# In[12]:


from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense


# In[13]:


model = Sequential()
model.add(Dense(1, activation="sigmoid"))


# In[14]:


model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])


# In[15]:


train = model.fit(X_train , y_train , validation_data=(X_test,y_test), epochs=100, verbose=1)


# In[16]:


y_ann = model.predict(X_test)


# In[17]:


y_ann = y_ann.flatten()


# In[18]:


y_ann


# In[19]:


for y_a in y_ann :
    if y_a > 0.5 :
        y_a = 1
    else :
        y_a =0


# In[20]:


y_ann = np.round(y_ann)


# In[21]:


y_ann


# In[22]:


accuracy_score(y_test, y_ann)


# In[23]:


confusion_matrix(y_test, y_ann)


# In[24]:


def plot_scores(train) :
    accuracy = train.history['accuracy']
    val_accuracy = train.history['val_accuracy']
    epochs = range(len(accuracy))
    plt.plot(epochs, accuracy, 'b', label='Score apprentissage')
    plt.plot(epochs, val_accuracy, 'r', label='Score validation')
    plt.title('Scores')
    plt.legend()
    plt.show()


# In[25]:


plot_scores(train)

